import requests

response = requests.get("https://gitlab.com/api/v4/users/Afamchidolue/projects")
my_projects = response.json()

# print the whole objects list
print(my_projects)
print(type(my_projects))

# print just the names and urls
for project in my_projects:
    print (f"Project Name: {project['name']}\n Project Url: {project['web_url']}\n")


